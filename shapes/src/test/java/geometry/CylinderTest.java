/**
 * Complete Test code for the Cylinder class
 * @author Nihel Madani-Fouatih
 * @version 10/16/2023
 */

package geometry;

import org.junit.Test;
import static org.junit.Assert.*;


public class CylinderTest
{

    @Test 
     public void testConstructorNegativeValues()
    {
        try{
            Cylinder cylinder = new Cylinder(-1, -26);
            fail("Exception should have been thrown as we cannot pass negative values");
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
       
    }
    
    @Test 
    public void testGetRadius()
    {
        Cylinder cylinder1= new Cylinder(15, 10.3);
        assertEquals(15,cylinder1.getRadius(), 001);
    }

     @Test 
    public void testGetHeight()
    {
        Cylinder cylinder2= new Cylinder(15, 10.3);
        assertEquals(10.3,cylinder2.getHeight(), 001);
    }

    @Test 
     public void testGetVolume()
    {
        Cylinder cylinder3= new Cylinder(2.3, 4.2);
        double expectedVolume= (Math.PI * Math.pow(2.3, 2)* 4.2);
        assertEquals(expectedVolume,cylinder3.getVolume(),001);
    }

      @Test 
     public void testGetSurfaceArea()
    {
        Cylinder cylinder4= new Cylinder(1.2, 3.2);
        double expectedSurfaceArea= (2 * Math.PI * Math.pow(1.2, 2) + (2 * Math.PI * 1.2 *3.2));
        assertEquals(expectedSurfaceArea,cylinder4.getSurfaceArea(),001);
    }

}
