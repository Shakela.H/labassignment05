/**
 * This is The unit test for Sphere
 * @author Shakela Hossain
 * @version 10/16/2023
 */
package geometry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * Unit test for Sphere.
 */
public class SphereTest {
 
    @Test
    public void testConstructorNegativeValues()
    {
        try{
            double rad = -2.5;
            Sphere sphere = new Sphere(rad);
            fail("Exception should have been thrown as we cannot pass negative values");
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void getVolume()
    {
        double rad = 2.5;
        Sphere sphere = new Sphere(rad);
        double expected = (4/3 * (Math.PI * Math.pow(rad,3)));
        assertEquals(expected, sphere.getVolume(), 001);
    }

    @Test
    public void getSurfaceArea()
    {
        double rad = 2.5;
        Sphere sphere = new Sphere(rad);
        double expected = 4 * Math.PI * Math.pow(rad, 2);
        assertEquals(expected,sphere.getSurfaceArea(), 001);
    }

    @Test
    public void getRadius()
    {
        double rad = 2.5;
        Sphere sphere = new Sphere(rad);
        assertEquals(rad,sphere.getRad(),001);
    }
}




