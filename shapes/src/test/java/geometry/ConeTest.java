/**
 * This is a Junit test class for the Cone class
 * @author Iana Feniuc
 * @version 10/17/2023
 */

 package geometry;

 import org.junit.Test;
 import static org.junit.Assert.*;


 public class ConeTest{
    @Test
    public void testConstructor(){
        try
        {
            Cone cone = new Cone(-6,-9);
            fail("The test failed since the exception hasn't been thrown since the input fields can't be negative!");
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Test 
    public void testHeight(){
        double height = 5;
        double radius = 3;
        Cone cone = new Cone(height,radius);
        assertEquals(height,cone.getHeight(), 001);
    }
     @Test 
    public void testRadius(){
        double height = 5;
        double radius = 3;
        Cone cone = new Cone(height,radius);
        assertEquals(radius,cone.getRadius(), 001);
    }

     @Test 
    public void testVolume(){
        double height = 5;
        double radius = 3;
        Cone cone = new Cone(height,radius);
        double expectedResult= Math.PI*(Math.pow(radius,2))*(height/2);
        assertEquals(expectedResult,cone.getVolume(), 001);
    }

    @Test 
    public void testSurfaceArea(){
        double height = 5;
        double radius = 3;
        Cone cone = new Cone(height,radius);
        double expectedResult= Math.PI*radius*(radius+Math.sqrt(Math.pow(height,2)+Math.pow(radius,2)));
        assertEquals(expectedResult,cone.getSurfaceArea(), 001);
    }

 }