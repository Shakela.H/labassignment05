package geometry;

interface Shape3d{
    double getVolume();
    double getSurfaceArea();
}