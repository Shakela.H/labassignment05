package geometry;
/**
 * This is a production code for the Cone class
 * @author Iana Feniuc
 * @version 10/17/2023
 */
public class Cone implements Shape3d{
    private double height ;
    private double radius ;

    /**
    *  Constructor initilaizes the fields: radius and height!
    *
    *  @param  height 
    *  @param  radius
    *  @return nothing
    */
    public Cone(double height, double radius){
        if (height<0 || radius<0)
        {
            throw new IllegalArgumentException("Height or radius can't be negative!");
        }
        this.height = height;
        this.radius = radius;
    }
    /**
    *  Returns the height of the cone
    *
    *  @param  none
    *  @return The value of the height
    */
    public double getHeight(){
        return this.height;
    }
    
    /**
    *  Returns the radius of the cone
    *
    *  @param  none
    *  @return The value of the radius
    */
    public double getRadius(){
        return this.radius;
    }
    
    /**
    *  Returns the volume of the Cone
    *
    *  @param  none
    *  @return The value of the volume of the Cone
    */
    @Override
	public double getVolume() {
        return Math.PI*(Math.pow(this.radius,2))*(this.height/2);
    }

    /**
    *  Returns the surface area of the Cone
    *
    *  @param  none
    *  @return The value of the surface area of the Cone
    */

    @Override
	public double getSurfaceArea() {
        return Math.PI*this.radius*(this.radius+Math.sqrt(Math.pow(this.height,2)+Math.pow(this.radius,2)));
    }


}