/**
 * This is a skeleton class for the Cylinder class
 * @author Shakela Hossain
 * @version 10/15/2023
 */
package geometry;

/**
 * Represents a Cylinder in three-dimensional space.
 */

public class Cylinder implements Shape3d{
    private double radius;
    private double height;

    /**
     * Initializes a new instance of a Cylinder with the specified radius and height.
     * 
     * @param radius
     * @param height
     */
    public Cylinder(double radius, double height){
        // setting the values of radius and height when the cylinder object is first created
        if (radius < 0 || height < 0) {
            throw new IllegalArgumentException("Radius and height cannot be negative");
        }
        this.radius = radius;
        this.height = height;
    }
    
    /**
     *
     * @return the volume of the cylinder
     */
    public double getVolume(){

        return Math.PI*Math.pow(this.radius,2)*this.height;  
    }
    
    /**
     *
     * @return the surface area of the cylinder
     */
    public double getSurfaceArea(){
        return 2*Math.PI*Math.pow(this.radius,2) + 2*Math.PI*this.radius*this.height;
    }

    public double getHeight(){
        return this.height;
    }

    public double getRadius(){
        return this.radius;
    }

}
