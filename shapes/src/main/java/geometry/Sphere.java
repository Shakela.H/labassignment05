/**
 * Complete production code for the Sphere Class 
 * @author Nihel Madani-Fouatih
 * @version 10/16/2023
 */

package geometry;

public class Sphere implements Shape3d{
    private double radius;


    public Sphere(double rad)
    {
       if (rad <=0)
       {
            throw new IllegalArgumentException("The radius has to be a positive value");
       }
       this.radius=rad;
    }

     /**
     * @return the radius of the Sphere
     */
    public double getRad()
    {
        return this.radius;
    }
    
     /**
     * @return the Volume of the Sphere
     */
    public double getVolume()
    {
        return (4/3 * (Math.PI * Math.pow(this.radius,3)));
    }

     /**
     * @return the Surface Area of the Sphere
     */
    public double getSurfaceArea()
    {
         return (4 * Math.PI * Math.pow(this.radius,2));
    }
    
}
